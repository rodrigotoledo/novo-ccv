<?php
/**
 * Static content controller.
 *
 * This file will render views from views/Dashboard/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 */
class AdminController extends AppController {
  public $helpers = array(
    'Session',
    'Layout',
    'Html'      => array('className' => 'TwitterBootstrap.BootstrapHtml'),
    'Form'      => array('className' => 'TwitterBootstrap.BootstrapForm'),
    'Paginator' => array('className' => 'TwitterBootstrap.BootstrapPaginator'),
    'SimpleForm');

  public $components = array(
  'Session',
  'Cookie',
  // 'Auth'=> array(
  //   'loginAction' => array('controller' => 'Login', 'action' => 'index'),
  //   'loginRedirect' => array('controller' => 'RealEstates', 'action' => 'index', 'type' => 'Aluguel'),
  //   'logoutRedirect' => array('controller' => 'Login', 'action' => 'index'),
  //   'authenticate' => array(
  //     'Form' => array(
  //       'fields' => array('username' => 'email', 'password' => 'pass')
  //     )
  //   ),
  //   'authError' => 'Acesso negado, necessita fazer o login'
  // )
  );

  function beforeFilter(){
    $this->layout = 'admin';
  }

  protected function _buildTableSearchConditions($fields,$q, $exclusiveColumn = false)
  {
    if(empty($q)){
      return array();
    }else{
      $conditions = array();
      if(!empty($exclusiveColumn)){
        $conditions[$exclusiveColumn.' LIKE'] = '%'.$q.'%';
      }else{
        $dateAttributes = array('UserContact.birthday', 'DistributorInformation.initialized_at', 'SearchUserEmployee.admitted_at', 'SearchUserPartner.last_event_occur_at');
        foreach ($fields as $field) {
          if(strpos($q, '/') !== false && in_array($field,$dateAttributes)){
            $conditions['OR'][$field] = join('-',array_reverse(explode('/',$q)));
          }else{
            $conditions['OR'][$field] = '%'.$q.'%';
          }
        }
      }
      
      return $conditions;
    }
  }

  public function citiesByState()
  {
    $_City = ClassRegistry::init('City');
    $cities = array();
    if(!empty($this->request->query['state_id'])){
      $cities = $_City->find('list', array('conditions' => array(
        'City.state_id' => $this->request->query['state_id']
        )
      ));
    }
    echo json_encode($cities);
    die;
  }


  public function addressByZipcode()
  {
    $zipcode = @$this->request->query['zipcode'];
    $response = array();
    if(!empty($zipcode)){
      $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $zipcode);
      $cityResponse = (string) $reg->cidade;
      if(!empty($cityResponse)){
        $_City = ClassRegistry::init('City');
        $city =  $_City->find('first', array(
          'recursive' => -1,
          'conditions' => array('City.name' => $cityResponse))
        );
      }
      $response['success'] = (string) $reg->resultado;
      $addressParts[] = (string) $reg->tipo_logradouro;
      $addressParts[] = (string) $reg->logradouro;
      $addressParts[] = (string) $reg->bairro;
      $response['address'] =  join(', ', array_filter($addressParts));
      if(@$city){
        $response['city_id'] = (int) $city['City']['id'];
        $response['state_id'] = (int) $city['City']['state_id'];
        $response['cities'] = $_City->find('list', array('conditions' => array(
          'City.state_id' => $response['state_id']
          )
        ));
      }
    }
    echo json_encode($response);
    die;
  }

}
