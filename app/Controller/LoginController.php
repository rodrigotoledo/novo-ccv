<?php
/**
 * Static content controller.
 *
 * This file will render views from views/Login/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AdminController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 */
class LoginController extends AdminController {
  
  public function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allow('index');
  }

/**
 * Controller name
 *
 * @var string
 */
  public $name = 'Login';

/**
 * This controller does not use a model
 *
 * @var array
 */
  public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
  public function index() {
    $this->layout = 'login';
    if ($this->request->is('post') || $this->request->is('put')) {
      // debug($this->request->data);
      if ($this->Auth->login()) {
        return $this->redirect($this->Auth->redirectUrl());
      } else {
        $this->Session->setFlash(
          __('Dados incorretos, favor tentar novamente.'),
          'alert',
          array(
            'plugin' => 'TwitterBootstrap',
            'class' => 'alert-error'
          )
        );
      }
    }
  }

  public function logout() {
    $this->Cookie->delete('Auth.User');
    $this->redirect($this->Auth->logout());
  }
}
