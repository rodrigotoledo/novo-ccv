<?php
App::uses('AdminController', 'Controller');
/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AdminController {

	var $uses = array('Group');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Group->recursive = 0;
		$this->Group->order = 'Group.name';
		$conditions = $this->_buildTableSearchConditions(array('Group.name LIKE'),@$this->request->query['q']);
		$this->set('groups', $this->paginate('Group', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid %s', __('group')));
		}
		$this->set('group', $this->Group->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('group')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('group')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}

		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid %s', __('group')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Group->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('group')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('group')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Group->read(null, $id);
		}

		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid %s', __('group')));
		}
		if ($this->Group->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('group')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('group')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$distributorsToIgnore = $this->Group->GroupCompany->find('list', array(
			'fields' => array('GroupCompany.company_id', 'GroupCompany.group_id'),
			'conditions' => array('group_id <>' => (int) $this->Group->id)));
		$distributorsToIgnore   = array_keys($distributorsToIgnore);
		$distributorsToIgnore[] = 0;
		$distributors = $this->Group->Distributor->find('list', array('order' => 'name',
			'conditions' => array('not' => array('Distributor.id' => $distributorsToIgnore))));
		$manufacturers = $this->Group->Manufacturer->find('list', array('order' => 'name'));
		$this->set(compact('manufacturers', 'distributors'));
	}

	public function updateDistributorsDetails()
	{
		if(!is_array($this->request->query['distributorsIds'])){
			$distributorsDetails = array();
		}else{
			$distributorsIds = $this->request->query['distributorsIds'];
				$distributorsDetails = $this->Group->Company->find('all', array('recursive' => 1,
				'conditions' => array('Company.id' => $distributorsIds)));
			foreach($distributorsDetails as &$distributorDetail){
				$companyConfiguration = $this->Group->Company->CompanyConfiguration->findById($distributorDetail['CompanyConfiguration']['id']);
				$aov = $this->Group->Company->CompanyConfiguration->Aov->findById($companyConfiguration['Aov']['id']);
				$distributorDetail['Aov'] = $aov['Aov'];
				$distributorDetail['Region'] = $companyConfiguration['Region'];
				$distributorDetail['UserEmployee'] = $aov['UserEmployee'];
				// debug($distributorDetail['Aov']);
			}
		}
		
		$this->set('distributorsDetails', $distributorsDetails);
		$this->render('/Elements/Group/distributorsDetails', 'ajax');
	}

}
