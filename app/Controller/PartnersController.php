<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property Partner $Partner
 */
class PartnersController extends AdminController {

	var $uses = array('Partner','SearchPartner');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchPartner->recursive = 0;
		$this->SearchPartner->order = 'SearchPartner.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchPartner.name LIKE',
			'SearchPartner.cnpj LIKE',
			'State.name LIKE',
			'City.name LIKE',
			'CompanyConfiguration.status LIKE',
			'SearchPartner.fiscal_name LIKE'),@$this->request->query['q']);
		$this->set('partners', $this->paginate('SearchPartner', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Partner->id = $id;
		if (!$this->Partner->exists()) {
			throw new NotFoundException(__('Invalid %s', __('partner')));
		}
		$this->set('partner', $this->Partner->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Partner->create();
			if ($this->Partner->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('partner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('partner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Partner->id = $id;
		if (!$this->Partner->exists()) {
			throw new NotFoundException(__('Invalid %s', __('partner')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Partner->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('partner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('partner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Partner->read(null, $id);
		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Partner->id = $id;
		if (!$this->Partner->exists()) {
			throw new NotFoundException(__('Invalid %s', __('partner')));
		}
		if ($this->Partner->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('partner')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('partner')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->Partner->CompanyAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['CompanyAddress']['state_id'])){
			$cities = $this->Partner->CompanyAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['CompanyAddress']['state_id']
				)
			));
		}

		$this->set(compact('states', 'cities'));
	}

}
