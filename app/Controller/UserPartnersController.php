<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property UserPartner $UserPartner
 */
class UserPartnersController extends AdminController {
	var $uses = array('UserPartner','SearchUserPartner', 'FullRole');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchUserPartner->recursive = 0;
		$this->SearchUserPartner->order = 'SearchUserPartner.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchUserPartner.name LIKE',
			'UserContact.birthday',
			'UserContact.cpf LIKE',
			'Company.name LIKE',
			'SearchUserPartner.email LIKE',
			'ProfessionalPosition.name LIKE',
			'SearchUserPartner.last_event_name LIKE',
			'SearchUserPartner.last_event_occur_at',
			'Role.name LIKE',
			'UserConfiguration.status LIKE',
			),@$this->request->query['q'], @$this->request->query['column']);
		$this->set('userPartners', $this->paginate('SearchUserPartner', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->UserPartner->id = $id;
		if (!$this->UserPartner->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userPartner')));
		}
		$this->set('userPartner', $this->UserPartner->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserPartner->create();
			if ($this->UserPartner->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('userPartner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('userPartner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->UserPartner->id = $id;
		$this->UserPartner->recursive = 2;
		if (!$this->UserPartner->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userPartner')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->UserPartner->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('userPartner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('userPartner')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);

				$userProfessionalEventsFromForm = @$this->request->data['UserProfessionalEvent'];
				$this->request->data['UserProfessionalEvent'] = array();

				
				if(count($userProfessionalEventsFromForm)){
					foreach($userProfessionalEventsFromForm as $i => $userProfessionalEvent){
						if(!array_key_exists('id', $userProfessionalEvent)){
							$professionalEvent = $this->UserPartner->UserProfessionalEvent->ProfessionalEvent->findById($userProfessionalEvent['professional_event_id']);
							$this->request->data['UserProfessionalEvent'][$i] = array(
								'ProfessionalEvent' => $professionalEvent['ProfessionalEvent'],
								'professional_event_id' => $userProfessionalEvent['professional_event_id'],
								'occur_at' => $userProfessionalEvent['occur_at']
							);
						}else{
							$userProfessionalEventIdsFromForm[] = $userProfessionalEvent['id'];
						}
					}
				}
				$userProfessionalEventIdsFromForm[] = 0;
				
				$userProfessionalEvents = $this->UserPartner->UserProfessionalEvent->find('all', array(
					'conditions' => array(
						'UserProfessionalEvent.user_id' => $id,
						'UserProfessionalEvent.id' => $userProfessionalEventIdsFromForm)
					)
				);
				foreach ($userProfessionalEvents as $userProfessionalEvent) {
					$this->request->data['UserProfessionalEvent'][] = array_merge(
						$userProfessionalEvent['UserProfessionalEvent'],
						array('ProfessionalEvent' => $userProfessionalEvent['ProfessionalEvent'])
					);
				}
			}
		} else {
			$this->request->data = $this->UserPartner->read(null, $id);
		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->UserPartner->id = $id;
		if (!$this->UserPartner->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userPartner')));
		}
		if ($this->UserPartner->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('userPartner')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('userPartner')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->UserPartner->UserAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['UserAddress']['state_id'])){
			$cities = $this->UserPartner->UserAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['UserAddress']['state_id']
				)
			));
		}

		$professionalEvents = $this->UserPartner->UserProfessionalEvent->ProfessionalEvent->find('list');
		$partners = $this->UserPartner->Partner->find('list');
		$roles = $this->FullRole->find('list', array('conditions' => array('FullRole.user_type' => 'Parceiro')));
		$tshirts = $this->UserPartner->UserGarb->Tshirt->find('list');
		$shoes = $this->UserPartner->UserGarb->Shoe->find('list');
		$educationFormationGroups = $this->UserPartner->EducationFormation->EducationFormationGroup->find('all');
		$manufacturers = $this->UserPartner->Manufacturer->find('list');
		$professionalPositions = $this->UserPartner->UserProfessionalInformation->ProfessionalPosition->find('list');
		$userProfessionalEvents = @$this->request->data['UserProfessionalEvent'];
		$lastUserProfessionalEvent = false;
		if(!empty($userProfessionalEvents)){
			usort($userProfessionalEvents, function($a, $b) {
				return strtotime($a['occur_at']) - strtotime($b['occur_at']);
			});
			$lastUserProfessionalEvent = array_shift(array_values(array_reverse($userProfessionalEvents)));
		}

		$userInactiveReasons = $this->UserPartner->UserConfiguration->UserInactiveReason->find('list');
		$userInactiveMotives = $this->UserPartner->UserConfiguration->UserInactiveMotive->find('list');
		$this->set(compact('partners', 'roles', 'states', 'cities', 'tshirts',
			'shoes', 'educationFormationGroups', 'manufacturers', 'professionalPositions',
			'userInactiveReasons', 'userInactiveMotives',
			'professionalEvents', 'lastUserProfessionalEvent'));
	}
}
