<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property Administrator $Administrator
 */
class AdministratorsController extends AdminController {
	var $uses = array('Administrator','SearchAdministrator');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchAdministrator->recursive = 0;
		$this->SearchAdministrator->order = 'SearchAdministrator.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchAdministrator.name LIKE',
			'UserContact.birthday',
			'UserContact.cpf LIKE',
			'UserContact.phone_comercial LIKE',
			'UserContact.mobile LIKE',
			'City.name LIKE',
			'State.short_name LIKE',
			'Role.name LIKE',
			'UserConfiguration.status LIKE',
			),@$this->request->query['q'], @$this->request->query['column']);
		$this->set('administrators', $this->paginate('SearchAdministrator', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Administrator->id = $id;
		if (!$this->Administrator->exists()) {
			throw new NotFoundException(__('Invalid %s', __('administrator')));
		}
		$this->set('administrator', $this->Administrator->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Administrator->create();
			if ($this->Administrator->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('administrator')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('administrator')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Administrator->id = $id;
		if (!$this->Administrator->exists()) {
			throw new NotFoundException(__('Invalid %s', __('administrator')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Administrator->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('administrator')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('administrator')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Administrator->read(null, $id);

		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Administrator->id = $id;
		if (!$this->Administrator->exists()) {
			throw new NotFoundException(__('Invalid %s', __('administrator')));
		}
		if ($this->Administrator->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('administrator')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('administrator')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->Administrator->UserAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['UserAddress']['state_id'])){
			$cities = $this->Administrator->UserAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['UserAddress']['state_id']
				)
			));
		}

		$roles = $this->Administrator->UserConfiguration->Role->find('list');
		$tshirts = $this->Administrator->UserGarb->Tshirt->find('list');
		$shoes = $this->Administrator->UserGarb->Shoe->find('list');
		$educationFormationGroups = $this->Administrator->EducationFormation->EducationFormationGroup->find('all');
		$userInactiveReasons = $this->Administrator->UserConfiguration->UserInactiveReason->find('list');
		$userInactiveMotives = $this->Administrator->UserConfiguration->UserInactiveMotive->find('list');
		$this->set(compact('roles', 'states', 'cities', 'tshirts', 'shoes',
			'userInactiveReasons', 'userInactiveMotives',
			'educationFormationGroups'));
	}

}
