<?php
App::uses('AppModel', 'Model');
/**
 * UserProfessionalDepartment Model
 *
 * @property User $User
 * @property ProfessionalDepartment $ProfessionalDepartment
 */
class UserProfessionalDepartment extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProfessionalDepartment' => array(
			'className' => 'ProfessionalDepartment',
			'foreignKey' => 'professional_department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
