<?php
App::uses('AppModel', 'Model');
/**
 * Tshirt Model
 *
 * @property UserGarb $UserGarb
 */
class Tshirt extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'UserGarb' => array(
			'className' => 'UserGarb',
			'foreignKey' => 'tshirt_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
