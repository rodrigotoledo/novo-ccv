<?php
App::uses('AppModel', 'Model');
/**
 * Group Model
 *
 * @property CompanyConfiguration $CompanyConfiguration
 * @property GroupCompany $GroupCompany
 * @property GroupManufacturer $GroupManufacturer
 */
class Group extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyConfiguration' => array(
			'className' => 'CompanyConfiguration',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'GroupCompany' => array(
			'className' => 'GroupCompany',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'GroupManufacturer' => array(
			'className' => 'GroupManufacturer',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	var $hasAndBelongsToMany = array(
	'Manufacturer' => array(
			'className' => 'Manufacturer',
			'joinTable' => 'group_manufacturers',
			'foreignKey' => 'group_id',
			'associationForeignKey' => 'manufacturer_id',
			'with' => 'GroupManufacturer',
			'unique' => 'keepExisting',
		),
	'Distributor' => array(
			'className' => 'Distributor',
			'joinTable' => 'group_companies',
			'foreignKey' => 'group_id',
			'associationForeignKey' => 'company_id',
			'with' => 'GroupCompany',
			'unique' => 'keepExisting',
		),
	);

	public function afterSave($options)
	{
		$this->Distributor->CompanyConfiguration->updateAll(
			array('CompanyConfiguration.group_id' => NULL),
			array('CompanyConfiguration.group_id' => $this->id)
		);
		if(is_array($this->data['Distributor']['Distributor'])){
			$companiesConfigurations = $this->Distributor->CompanyConfiguration->find('list',array(
				'conditions' => array('CompanyConfiguration.company_id' => $this->data['Distributor']['Distributor'])
			));
			$this->Distributor->CompanyConfiguration->updateAll(
				array('CompanyConfiguration.group_id' => $this->id),
				array('CompanyConfiguration.id' => array_keys($companiesConfigurations))
			);
		}
		return true;
	}
}
