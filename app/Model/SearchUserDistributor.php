<?php
App::uses('AppModel', 'Model');
/**
 * SearchUserDistributor Model
 *
 * @property Company $Company
 * @property State $State
 * @property City $City
 * @property Role $Role
 */
class SearchUserDistributor extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
    'ProfessionalOccupation' => array(
      'className' => 'ProfessionalOccupation',
      'foreignKey' => 'professional_occupation_id',
    ),
    'ProfessionalPosition' => array(
      'className' => 'ProfessionalPosition',
      'foreignKey' => 'professional_position_id',
    ),
	);


	/**
 * hasOne associations
 *
 * @var array
 */
  public $hasOne = array(
    'UserContact' => array(
      'className' => 'UserContact',
      'foreignKey' => 'user_id',
    ),
    'UserConfiguration' => array(
      'className' => 'UserConfiguration',
      'foreignKey' => 'user_id',
    ),
    'UserAddress' => array(
      'className' => 'UserAddress',
      'foreignKey' => 'user_id',
    ),
  );
}
