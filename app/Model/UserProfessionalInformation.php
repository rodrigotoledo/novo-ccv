<?php
App::uses('AppModel', 'Model');
/**
 * UserProfessionalInformation Model
 *
 * @property User $User
 * @property ProfessionalOccupation $ProfessionalOccupation
 * @property ProfessionalPosition $ProfessionalPosition
 * @property LastProfessionalPosition $LastProfessionalPosition
 */
class UserProfessionalInformation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		// 'professional_occupation_id' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		'message' => '* Deve ser preenchida',
		// 		//'allowEmpty' => false,
		// 		'required' => true,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ), Atividade principal
		'professional_position_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchida',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		// 'admitted_at' => array(
		// 	'date' => array(
		// 		'rule' => array('date'),
		// 		'message' => '* Deve ser preenchido',
		// 		//'allowEmpty' => false,
		// 		// 'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'change_professional_position_at' => array(
		// 	'date' => array(
		// 		'rule' => array('date'),
		// 		'message' => '* Deve ser preenchido',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ), Cargo Anterior
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProfessionalOccupation' => array(
			'className' => 'ProfessionalOccupation',
			'foreignKey' => 'professional_occupation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProfessionalPosition' => array(
			'className' => 'ProfessionalPosition',
			'foreignKey' => 'professional_position_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'LastProfessionalPosition' => array(
			'className' => 'ProfessionalPosition',
			'foreignKey' => 'last_professional_position_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasAndBelongsToMany = array(
	'ProfessionalDepartment' => array(
		'className' => 'ProfessionalDepartment',
		'joinTable' => 'user_professional_departments',
		'foreignKey' => 'user_id',
		'associationForeignKey' => 'professional_department_id',
		'with' => 'UserProfessionalDepartment',
		'unique' => 'keepExisting',
		)
	);

	public function beforeValidate($options = array()) {
    $this->dateFormatBeforeSave($this->data['UserProfessionalInformation']['admitted_at']);
 	  return true;
	}


	public function beforeSave($options)
	{
		if(!empty($this->id)){
			$lastUserProfessionalInformation = $this->findById($this->id);
			if($lastUserProfessionalInformation['UserProfessionalInformation']['professional_position_id'] != $this->data['UserProfessionalInformation']['professional_position_id']){
				$this->data['UserProfessionalInformation']['last_professional_position_id']   = $lastUserProfessionalInformation['UserProfessionalInformation']['professional_position_id'];
				$this->data['UserProfessionalInformation']['change_professional_position_at'] = date('Y-m-d');
			}
		}else{
			$this->data['UserProfessionalInformation']['last_professional_position_id']   = $this->data['UserProfessionalInformation']['professional_position_id'];
			$this->data['UserProfessionalInformation']['change_professional_position_at'] = date('Y-m-d');
		}
		
		return true;
	}
}
