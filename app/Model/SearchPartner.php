<?php
App::uses('AppModel', 'Model');
/**
 * SearchPartner Model
 *
 * @property Manufacturer $Manufacturer
 * @property State $State
 * @property City $City
 */
class SearchPartner extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Manufacturer' => array(
			'className' => 'Manufacturer',
			'foreignKey' => 'manufacturer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


	public $hasOne = array(
		'CompanyConfiguration' => array(
			'className' => 'CompanyConfiguration',
			'foreignKey' => 'company_id',
		),
	);
}
