<?php
App::uses('AppModel', 'Model');
/**
 * FullRole Model
 *
 */
class FullRole extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
