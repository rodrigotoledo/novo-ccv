<?php
App::uses('AppModel', 'Model');
/**
 * DistributorInformation Model
 *
 * @property Company $Company
 */
class DistributorInformation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'dlr_toyos' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'initialized_at' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeValidate($options = array()) {
    $this->dateFormatBeforeSave($this->data['DistributorInformation']['initialized_at']);
	  return true;
	}
}
