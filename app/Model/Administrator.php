<?php
App::uses('AppModel', 'Model');
/**
 * Administrator Model
 *
 * @property Company $Company
 */
class Administrator extends AppModel {

/**
 * Display field
 *
 * @var string
 */
public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
public $validate = array(
	'name' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'email' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			'message' => '* Deve ser preenchido',
              //'allowEmpty' => false,
              //'required' => false,
              //'last' => false, // Stop validation after this rule
              //'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'email' => array(
			'rule' => array('email'),
			'message' => 'Deve ser um email válido.',
              //'allowEmpty' => false,
              //'required' => false,
              //'last' => false, // Stop validation after this rule
              //'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Já existe um registro com esse valor',
			'allowEmpty' => true,
			),
		),
	'confirm_email' => array(
		'equalToEmail' => array(
			'rule' => array('equalToEmail'),
			'message' => '* Email não é igual',
              //'allowEmpty' => false,
			'required' => false,
              //'last' => false, // Stop validation after this rule
			'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	// 'pass' => array(
	// 	'notempty' => array(
	// 		'rule' => array('notempty'),
	// 		'message' => '* Deve ser preenchido',
	// 		'allowEmpty' => false,
 //              //'required' => false,
 //              //'last' => false, // Stop validation after this rule
 //              'on' => 'create', // Limit validation to 'create' or 'update' operations
 //              ),
	// 	'minLength' => array(
	// 		'rule' => array('minLength',3),
	// 		'message' => 'Mínimo de 3 caracteres.',
	// 		'allowEmpty' => true,
 //              //'required' => false,
 //              //'last' => false, // Stop validation after this rule
 //              // 'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 'confirm_password' => array(
	// 	'equalToPassword' => array(
	// 		'rule' => array('equalToPassword'),
	// 		'message' => 'Não confere com a senha.',
 //              //'allowEmpty' => false,
	// 		'required' => false,
 //              //'last' => false, // Stop validation after this rule
 //              // 'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 'password' => array(
	// 	'notempty' => array(
	// 		'rule' => array('notempty'),
	// 		'message' => '* Deve ser preenchido',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	'user_type' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
public $hasOne = array(
	'UserContact' => array(
		'className' => 'UserContact',
		'foreignKey' => 'user_id',
		),
	'UserConfiguration' => array(
		'className' => 'UserConfiguration',
		'foreignKey' => 'user_id',
		),
	'UserAddress' => array(
		'className' => 'UserAddress',
		'foreignKey' => 'user_id',
		),
	'UserGarb' => array(
		'className' => 'UserGarb',
		'foreignKey' => 'user_id',
		),
	'UserEducationInformation' => array(
		'className' => 'UserEducationInformation',
		'foreignKey' => 'user_id',
		),
	);

var $hasAndBelongsToMany = array(
	'EducationFormation' => array(
		'className' => 'EducationFormation',
		'joinTable' => 'user_education_formations',
		'foreignKey' => 'user_id',
		'associationForeignKey' => 'education_formation_id',
		'with' => 'UserEducationFormation',
		'unique' => 'keepExisting',
		)
	);


public $actsAs = array(
	'Upload.Upload' => array(
		'photo' => array(
			'thumbnailSizes' => array(
				'640x480' => '640x480',
				'100x100' => '100x100'
				),
			'thumbnailQuality' => 90
			)
		)
	);
}
