<?php
App::uses('AppModel', 'Model');
/**
 * SearchUserEmployee Model
 *
 * @property Company $Company
 * @property ProfessionalPosition $ProfessionalPosition
 * @property Role $Role
 */
class SearchUserEmployee extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProfessionalPosition' => array(
      'className' => 'ProfessionalPosition',
      'foreignKey' => 'professional_position_id',
    ),
	);


	/**
 * hasOne associations
 *
 * @var array
 */
  public $hasOne = array(
    'UserContact' => array(
      'className' => 'UserContact',
      'foreignKey' => 'user_id',
    ),
    'UserConfiguration' => array(
      'className' => 'UserConfiguration',
      'foreignKey' => 'user_id',
    ),
    'UserAddress' => array(
      'className' => 'UserAddress',
      'foreignKey' => 'user_id',
    ),
  );
}
