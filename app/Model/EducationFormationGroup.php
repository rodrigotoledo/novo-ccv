<?php
App::uses('AppModel', 'Model');
/**
 * EducationFormationGroup Model
 *
 * @property EducationFormation $EducationFormation
 */
class EducationFormationGroup extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'EducationFormation' => array(
			'className' => 'EducationFormation',
			'foreignKey' => 'education_formation_group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
