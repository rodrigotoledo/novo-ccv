<?php
App::uses('AppModel', 'Model');
/**
 * UserInactiveMotive Model
 *
 */
class UserInactiveMotive extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
