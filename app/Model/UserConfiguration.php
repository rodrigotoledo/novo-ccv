<?php
App::uses('AppModel', 'Model');
/**
 * UserConfiguration Model
 *
 * @property User $User
 * @property Role $Role
 */
class UserConfiguration extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'role_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_inactive_reason_id' => array(
			'validateInactiveReason' => array(
				'rule' => array('validateInactiveReason'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				// 'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_inactive_motive_id' => array(
			'validateInactiveMotive' => array(
				'rule' => array('validateInactiveMotive'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				// 'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'UserInactiveReason' => array(
			'className' => 'UserInactiveReason',
			'foreignKey' => 'user_inactive_reason_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'UserInactiveMotive' => array(
			'className' => 'UserInactiveMotive',
			'foreignKey' => 'user_inactive_motive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);


	public function beforeValidate($options = array()) {
    $this->dateFormatBeforeSave($this->data[$this->name]['inactive_at']);
	  return true;
	}

	function beforeSave($options)
	{
		if(empty($this->id)){
			$this->data[$this->name]['status'] = 'Em Experiência';
		}
		return true;
	}

	public function validateInactiveReason($check)
	{
		if($this->data[$this->name]['status'] != "Inativo"){
			return true;
		}

		if(empty($this->data[$this->name]['user_inactive_reason_id'])){
			return false;
		}
		return true;
	}

	public function validateInactiveMotive($check)
	{
		if($this->data[$this->name]['status'] != "Inativo"){
			return true;
		}

		if(empty($this->data[$this->name]['user_inactive_motive_id'])){
			return false;
		}
		return true;
	}
}
