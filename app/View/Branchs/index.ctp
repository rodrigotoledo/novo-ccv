<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Empresas</a>', 'Lista de Filiais')); ?>
<h3 class="heading"><?php echo __('Lista de %s', __('Filiais'));?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i> Cadastrar Nova',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>
	</div>
	<div class="span8">
		<?php echo $this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo @$this->request->query['q'] ?>" />
		<?php echo $this->Form->end(NULL) ?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('name',__('Nome Fantasia'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('fiscal_name',__('Razão Social'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('cnpj',__('CNPJ'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('State.name', __('Estado'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('City.name', __('Cidade'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('CompanyConfiguration.status', __('Status'));?></th>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
		<?php foreach ($branchs as $branch): ?>
			<tr>
				<td><?php echo h($branch['SearchBranch']['name']); ?>&nbsp;</td>
				<td><?php echo h($branch['SearchBranch']['fiscal_name']); ?>&nbsp;</td>
				<td><?php echo h($branch['SearchBranch']['cnpj']); ?>&nbsp;</td>
				<td><?php echo h($branch['State']['name']); ?>&nbsp;</td>
				<td><?php echo h($branch['City']['name']); ?>&nbsp;</td>
				<td><?php echo h($branch['CompanyConfiguration']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $branch['SearchBranch']['id']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>
					<?php echo $this->Form->postLink('<i class="icon-remove-sign align-icon"></i>', array('action' => 'delete', $branch['SearchBranch']['id']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo $this->BootstrapPaginator->pagination(); ?>
		</span>
	</div>
</div>
