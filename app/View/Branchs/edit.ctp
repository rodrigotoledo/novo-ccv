<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Empresas</a>', $this->Html->link('Lista de Filiais', array('action' => 'index')), 'Cadastro Filial')); ?>
<h3 class="heading">Empresas &raquo; Cadastro Filial</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Branch', array('class' => 'form-vertical well'));?>
			<?php echo $this->element('Forms/branch') ?>
			<?php echo $this->BootstrapForm->hidden('Branch.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyAddress.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyContact.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyConfiguration.id'); ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
