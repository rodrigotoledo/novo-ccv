<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Portal CCV - Sistema administrativo</title>
    <?php
    echo $this->Html->css(array(
    '/bootstrap/css/bootstrap.min',
    '/bootstrap/css/bootstrap-responsive.min',
    'blue',
    '/lib/qtip2/jquery.qtip.min',
    'style'
    ));
    ?>

    <!-- Favicons and the like (avoid using transparent .png) -->
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="icon.png" />

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body class="login_page">
      <?php echo $this->Session->flash() ?>
      <?php echo $this->fetch('content'); ?>
    
      <?php echo $this->Html->script(
      array( 
      'jquery.min.js',
      'jquery.actual.min.js',
      '/lib/validation/jquery.validate.min',
      '/bootstrap/js/bootstrap.min'
      ));
      ?>
      <script>
        $(document).ready(function(){
            
      //* boxes animation
      form_wrapper = $('.login_box');
            $('.linkform a,.link_reg a').on('click',function(e){
      var target  = $(this).attr('href'),
        target_height = $(target).actual('height');
      $(form_wrapper).css({
        'height'    : form_wrapper.height()
      }); 
      $(form_wrapper.find('form:visible')).fadeOut(400,function(){
        form_wrapper.stop().animate({
                        height  : target_height
                    },500,function(){
                        $(target).fadeIn(400);
                        $('.links_btm .linkform').toggle();
          $(form_wrapper).css({
            'height'    : ''
          }); 
                    });
      });
      e.preventDefault();
      });

      //* validation
      $('#login_form').validate({
      onkeyup: false,
      errorClass: 'error',
      validClass: 'valid',
      rules: {
        'data[User][email]': { required: true, minlength: 3 },
        'data[User][pass]': { required: true, minlength: 3 }
      },
      highlight: function(element) {
        $(element).closest('div').addClass("f_error");
      },
      unhighlight: function(element) {
        $(element).closest('div').removeClass("f_error");
      },
      errorPlacement: function(error, element) {
        $(element).closest('div').append(error);
      }
      });
        });
      </script>
  </body>
</html>
