<div class="login_box">
  <?php
  echo $this->Form->create('User', array(
  'id' => 'login_form',
  'inputDefaults' => array('label' => false, 'div' => false)
  ));
  ?>
    <div class="top_b">Portal CCV</div>
    <div class="alert alert-info alert-login">
      Bem vindo <strong>Sistema administrativo</strong>
    </div>
    <div class="cnt_b">
      <div class="formRow">
        <div class="input-prepend">
          <span class="add-on"><i class="icon-user"></i></span><input name="data[User][email]" placeholder="Usuário" type="text" id="UserEmail" />
        </div>
      </div>
      <div class="formRow">
        <div class="input-prepend">
          <span class="add-on"><i class="icon-lock"></i></span><input name="data[User][pass]" placeholder="Senha" type="password" id="UserPass" />
        </div>
      </div>
      <div class="formRow clearfix">
        <label class="checkbox"><input type="checkbox" /> Lembrar a senha</label>
      </div>
    </div>
    <div class="btm_b clearfix">
      <button class="btn btn-inverse pull-right" type="submit">Entrar</button>
      <span class="link_reg"><a href="#pass_form">Esqueceu sua senha?</a></span>
    </div>  
  <?php echo $this->Form->end();?>
  
  <?php
  echo $this->Form->create('User', array(
  'id' => 'pass_form',
  'style' => "display:none",
  'url' => array('action' => 'login'),
  'inputDefaults' => array('label' => false, 'div' => false)
  ));
  ?>
    <div class="top_b">¿Has perdido tu contraseña?</div>    
      <div class="alert alert-info alert-login">
      Introduzca su dirección de correo electrónico. Usted recibirá un enlace para crear una contraseña nueva por correo electrónico.
      </div>
    <div class="cnt_b">
      <div class="formRow clearfix">
        <div class="input-prepend">
          <span class="add-on">@</span><input type="text" placeholder="Su email" />
        </div>
      </div>
    </div>
    <div class="btm_b tac">
      <button class="btn btn-inverse" type="submit">Solicitar una nueva contraseña</button>
    </div>  
  <?php echo $this->Form->end();?>
</div>

<div class="links_b links_btm clearfix">
  <span class="linkform" style="display:none">No importa, <a href="#login_form">me envía de nuevo a la pantalla de inicio de sesión</a></span>
</div>
