<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', 'Lista de Regiões')); ?>
<h3 class="heading"><?php echo __('Lista de %s', __('Regiões'));?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i> Cadastrar Novo',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>
	</div>
	<div class="span8">
		<?php echo $this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo @$this->request->query['q'] ?>" />
		<?php echo $this->Form->end(NULL) ?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('name', __('Nome'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('aov_id', __('AOV'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('user_id', __('Consultor Regional'));?></th>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
		<?php foreach ($searchRegions as $searchRegion): ?>
			<tr>
				<td><?php echo h($searchRegion['SearchRegion']['name']); ?>&nbsp;</td>
				<td>
					<?php echo h($searchRegion['Aov']['name']); ?>
				</td>
				<td>
					<?php echo h($searchRegion['SearchRegion']['user_employee_name']); ?>
				</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $searchRegion['SearchRegion']['id']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>
					<?php echo $this->Form->postLink('<i class="icon-remove-sign align-icon"></i>', array('action' => 'delete', $searchRegion['SearchRegion']['id']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo $this->BootstrapPaginator->pagination(); ?>
		</span>

	</div>
</div>
