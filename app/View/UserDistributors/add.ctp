<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Usuários</a>', $this->Html->link('Lista de Distribuidores', array('action' => 'index')), 'Cadastro Distribuidor')); ?>
<h3 class="heading">Usuários &raquo; Cadastro Distribuidor</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('UserDistributor', array('class' => 'form-vertical well', 'type' => 'file'));?>
			<?php echo $this->element('Forms/userDistributor') ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
