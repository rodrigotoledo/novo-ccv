<?php echo $this->Html->breadcrumb(array(
    $this->Html->link('Lista de usuários',array('action' => 'index')),
    'Manutenção de usuário',
)); ?>

<?php echo $this->BootstrapForm->create('User', array('class' => 'form-horizontal', 'type' => 'file'));?>
	<?php echo $this->element('Users/form') ?>
<?php echo $this->BootstrapForm->end();?>