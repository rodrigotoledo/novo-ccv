<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('User');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($user['User']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($user['User']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Email'); ?></dt>
			<dd>
				<?php echo h($user['User']['email']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Password'); ?></dt>
			<dd>
				<?php echo h($user['User']['password']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Area'); ?></dt>
			<dd>
				<?php echo $this->Html->link($user['Area']['name'], array('controller' => 'areas', 'action' => 'view', $user['Area']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Nickname'); ?></dt>
			<dd>
				<?php echo h($user['User']['nickname']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Personal Email'); ?></dt>
			<dd>
				<?php echo h($user['User']['personal_email']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Phone'); ?></dt>
			<dd>
				<?php echo h($user['User']['phone']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Mobile'); ?></dt>
			<dd>
				<?php echo h($user['User']['mobile']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Birthday'); ?></dt>
			<dd>
				<?php echo h($user['User']['birthday']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Airbase'); ?></dt>
			<dd>
				<?php echo $this->Html->link($user['Airbase']['name'], array('controller' => 'airbases', 'action' => 'view', $user['Airbase']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Role'); ?></dt>
			<dd>
				<?php echo h($user['User']['role']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Photo'); ?></dt>
			<dd>
				<?php echo h($user['User']['photo']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Active'); ?></dt>
			<dd>
				<?php echo h($user['User']['active']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('User')), array('action' => 'edit', $user['User']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('User')), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Areas')), array('controller' => 'areas', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Area')), array('controller' => 'areas', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Airbases')), array('controller' => 'airbases', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Airbase')), array('controller' => 'airbases', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

