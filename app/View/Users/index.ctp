<?php echo $this->Html->breadcrumb(array(
    'Lista de usuários'
)); ?>
<div class="row-fluid">
  <div class="span12">
    <h3 class="heading">Lista de usuários <?php echo $this->Html->link('Novo usuário',array('action' => 'add'), array('class' => 'btn btn-primary pull-right')) ?></h3>
		<table class="table table-striped table-bordered" id="dt_a">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>Email</th>
				<th>Ativo?</th>
				<th class="actions">Ações</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
				<td><?php echo $user['User']['active'] ? 'Sim' : 'Não'; ?>&nbsp;</td>
				<td class="actions">
          <div class="btn-group">
  					<?php echo $this->Html->link('Editar', array('action' => 'edit', $user['User']['id']),array('class' => 'btn btn-mini')); ?>
  					<?php echo $this->Html->link('Apagar', array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-mini'), __('Deseja continuar com esta ação?')); ?>
          </div>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
		</table>
	</div>
</div>


<?php
$this->start('footerScripts');
  echo $this->Html->script(array( 
  '/lib/sticky/sticky.min',
  '/lib/datatables/jquery.dataTables.min',
  '/lib/datatables/extras/Scroller/media/js/Scroller.min',
  'gebo_datatables'
  ));
$this->end();
?>
