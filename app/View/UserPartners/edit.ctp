<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Usuários</a>', $this->Html->link('Lista de Parceiros', array('action' => 'index')), 'Cadastro Parceiro')); ?>
<h3 class="heading">Usuários &raquo; Cadastro Parceiro</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('UserPartner', array('class' => 'form-vertical well', 'type' => 'file'));?>
			<?php echo $this->element('Forms/userPartner') ?>
			<?php echo $this->BootstrapForm->hidden('UserPartner.id'); ?>
			<?php echo $this->BootstrapForm->hidden('UserAddress.id'); ?>
			<?php echo $this->BootstrapForm->hidden('UserGarb.id'); ?>
			<?php echo $this->BootstrapForm->hidden('UserContact.id'); ?>
			<?php echo $this->BootstrapForm->hidden('UserConfiguration.id'); ?>
			<?php echo $this->BootstrapForm->hidden('UserEducationInformation.id'); ?>
			<?php echo $this->BootstrapForm->hidden('UserProfessionalInformation.id'); ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
