<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Company');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($company['Company']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Manufacturer'); ?></dt>
			<dd>
				<?php echo $this->Html->link($company['Manufacturer']['name'], array('controller' => 'manufacturers', 'action' => 'view', $company['Manufacturer']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($company['Company']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Fiscal Name'); ?></dt>
			<dd>
				<?php echo h($company['Company']['fiscal_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Cnpj'); ?></dt>
			<dd>
				<?php echo h($company['Company']['cnpj']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Domain'); ?></dt>
			<dd>
				<?php echo h($company['Company']['domain']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('State Reference'); ?></dt>
			<dd>
				<?php echo h($company['Company']['state_reference']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Company Type'); ?></dt>
			<dd>
				<?php echo h($company['Company']['company_type']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created At'); ?></dt>
			<dd>
				<?php echo h($company['Company']['created_at']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Updated At'); ?></dt>
			<dd>
				<?php echo h($company['Company']['updated_at']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Company')), array('action' => 'edit', $company['Company']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Company')), array('action' => 'delete', $company['Company']['id']), null, __('Are you sure you want to delete # %s?', $company['Company']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Companies')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Company')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Manufacturers')), array('controller' => 'manufacturers', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Manufacturer')), array('controller' => 'manufacturers', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Company Addresses')), array('controller' => 'company_addresses', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Company Address')), array('controller' => 'company_addresses', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Company Configurations')), array('controller' => 'company_configurations', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Company Configuration')), array('controller' => 'company_configurations', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Company Contacts')), array('controller' => 'company_contacts', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Company Contact')), array('controller' => 'company_contacts', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Distributor Informations')), array('controller' => 'distributor_informations', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Distributor Information')), array('controller' => 'distributor_informations', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Company Addresses')); ?></h3>
	<?php if (!empty($company['CompanyAddress'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('State Id'); ?></th>
				<th><?php echo __('City Id'); ?></th>
				<th><?php echo __('Zipcode'); ?></th>
				<th><?php echo __('Address'); ?></th>
				<th><?php echo __('Address Complement'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($company['CompanyAddress'] as $companyAddress): ?>
			<tr>
				<td><?php echo $companyAddress['id'];?></td>
				<td><?php echo $companyAddress['company_id'];?></td>
				<td><?php echo $companyAddress['state_id'];?></td>
				<td><?php echo $companyAddress['city_id'];?></td>
				<td><?php echo $companyAddress['zipcode'];?></td>
				<td><?php echo $companyAddress['address'];?></td>
				<td><?php echo $companyAddress['address_complement'];?></td>
				<td><?php echo $companyAddress['created_at'];?></td>
				<td><?php echo $companyAddress['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'company_addresses', 'action' => 'view', $companyAddress['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'company_addresses', 'action' => 'edit', $companyAddress['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'company_addresses', 'action' => 'delete', $companyAddress['id']), null, __('Are you sure you want to delete # %s?', $companyAddress['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Company Address')), array('controller' => 'company_addresses', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Company Configurations')); ?></h3>
	<?php if (!empty($company['CompanyConfiguration'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Aov Id'); ?></th>
				<th><?php echo __('Region Id'); ?></th>
				<th><?php echo __('Group Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($company['CompanyConfiguration'] as $companyConfiguration): ?>
			<tr>
				<td><?php echo $companyConfiguration['id'];?></td>
				<td><?php echo $companyConfiguration['company_id'];?></td>
				<td><?php echo $companyConfiguration['aov_id'];?></td>
				<td><?php echo $companyConfiguration['region_id'];?></td>
				<td><?php echo $companyConfiguration['group_id'];?></td>
				<td><?php echo $companyConfiguration['created_at'];?></td>
				<td><?php echo $companyConfiguration['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'company_configurations', 'action' => 'view', $companyConfiguration['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'company_configurations', 'action' => 'edit', $companyConfiguration['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'company_configurations', 'action' => 'delete', $companyConfiguration['id']), null, __('Are you sure you want to delete # %s?', $companyConfiguration['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Company Configuration')), array('controller' => 'company_configurations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Company Contacts')); ?></h3>
	<?php if (!empty($company['CompanyContact'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Contact'); ?></th>
				<th><?php echo __('Email'); ?></th>
				<th><?php echo __('Site'); ?></th>
				<th><?php echo __('Phone Comercial1'); ?></th>
				<th><?php echo __('Phone Comercial2'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($company['CompanyContact'] as $companyContact): ?>
			<tr>
				<td><?php echo $companyContact['id'];?></td>
				<td><?php echo $companyContact['company_id'];?></td>
				<td><?php echo $companyContact['contact'];?></td>
				<td><?php echo $companyContact['email'];?></td>
				<td><?php echo $companyContact['site'];?></td>
				<td><?php echo $companyContact['phone_comercial1'];?></td>
				<td><?php echo $companyContact['phone_comercial2'];?></td>
				<td><?php echo $companyContact['created_at'];?></td>
				<td><?php echo $companyContact['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'company_contacts', 'action' => 'view', $companyContact['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'company_contacts', 'action' => 'edit', $companyContact['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'company_contacts', 'action' => 'delete', $companyContact['id']), null, __('Are you sure you want to delete # %s?', $companyContact['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Company Contact')), array('controller' => 'company_contacts', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Distributor Informations')); ?></h3>
	<?php if (!empty($company['DistributorInformation'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Dlr Toyos'); ?></th>
				<th><?php echo __('Initialized At'); ?></th>
				<th><?php echo __('Have Tv'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($company['DistributorInformation'] as $distributorInformation): ?>
			<tr>
				<td><?php echo $distributorInformation['id'];?></td>
				<td><?php echo $distributorInformation['company_id'];?></td>
				<td><?php echo $distributorInformation['dlr_toyos'];?></td>
				<td><?php echo $distributorInformation['initialized_at'];?></td>
				<td><?php echo $distributorInformation['have_tv'];?></td>
				<td><?php echo $distributorInformation['created_at'];?></td>
				<td><?php echo $distributorInformation['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'distributor_informations', 'action' => 'view', $distributorInformation['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'distributor_informations', 'action' => 'edit', $distributorInformation['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'distributor_informations', 'action' => 'delete', $distributorInformation['id']), null, __('Are you sure you want to delete # %s?', $distributorInformation['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Distributor Information')), array('controller' => 'distributor_informations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Users')); ?></h3>
	<?php if (!empty($company['User'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Name'); ?></th>
				<th><?php echo __('Email'); ?></th>
				<th><?php echo __('Photo'); ?></th>
				<th><?php echo __('Password'); ?></th>
				<th><?php echo __('User Type'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($company['User'] as $user): ?>
			<tr>
				<td><?php echo $user['id'];?></td>
				<td><?php echo $user['name'];?></td>
				<td><?php echo $user['email'];?></td>
				<td><?php echo $user['photo'];?></td>
				<td><?php echo $user['password'];?></td>
				<td><?php echo $user['user_type'];?></td>
				<td><?php echo $user['company_id'];?></td>
				<td><?php echo $user['created_at'];?></td>
				<td><?php echo $user['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
