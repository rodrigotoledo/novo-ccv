<?php

class UsersHelper extends AppHelper{
  public function phoneDDD()
  {
    return array(
    2,     32,    33,    34,
    35,    41,    42,    43,
    45,    51,    52,    53,
    55,    57,    58,    61,
    63,    64,    65,    67,
    71,    72,    73,    75
    );
  }


  public function mobileDDD()
  {
    return array(
      5,    6,    7,    8,    9
    );
  }

  public function roles()
  {
    return array(
      'AMC' => 'AMC: Administrador de mejora continua',
      'FMC' => 'FMC: Focal Point de mejora continua',
      'SP'  => 'SP: Sponsor',
      'GSP' => 'GSP: Sponsor General'
    );
  }
  
  public function photoPath($path,$filename,$size = '80x60')
  {
    if($size){
      $filename = $size.'_'.$filename;
    }
    return '/files/user/photo/'.$path.'/'.$filename;
  }
}