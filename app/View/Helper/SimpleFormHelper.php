<?
class SimpleFormHelper extends Helper{
  var $helpers = array('Time','Html', 'Layout');

  public function multiSelectAssociationIds($association)
  {
    $keys = @array_values($this->Html->value($association));
    $ids = array();
    if($keys){
      foreach ($keys as $key) {
        $ids[] = (int) $key;
      }
    }
    return $ids;
  }

  public function fieldIsInvalid($invalidFields, $fields)
  {
    if(!is_array($fields)){
      $fields = array($fields);
    }
    foreach ($fields as $field) {
      if(array_key_exists($field,$invalidFields)){
        return true;
      }
    }
    return false;
  }

  public function formatDateFromModel($value)
  {
    if (!empty($value)) {
      if(strpos($value, '-') !== false){
        return $this->Time->format($value, '%d/%m/%Y');
      }else{
        return $value;
      }
    }else{
      return '';
    }
  }

  public function distributorsStatusOptions()
  {
    return array('Ativo' => 'Ativo', 'Inativo' => 'Inativo');
  }

  public function administratorsStatusOptions()
  {
    return array('Ativo' => 'Ativo', 'Inativo' => 'Inativo');
  }

  public function userDistributorsStatusOptions()
  {
    return array('Ativo' => 'Ativo', 'Inativo' => 'Inativo',
      'Em Revisão' => 'Em Revisão', 'Em Experiência' => 'Em Experiência',
      'Ausência Temporária' => 'Ausência Temporária');
  }

  public function usersLiteraciesOptions()
  {
    return array('Ensino Fundamental' => 'Ensino Fundamental',
      'Ensino Médio' => 'Ensino Médio',
      'Ensino Superior Incompleto' => 'Ensino Superior Incompleto',
      'Ensino Superior Completo' => 'Ensino Superior Completo',
    );
  }

  public function searchDistributorsOptions()
  {
    // &raquo;&raquo; 
    return array('' => 'buscar em todos os campos',
      'Aov.name' => 'AOV',
      'Region.name' => 'Região',
      'Group.name' => 'Grupo',
      'DistributorInformation.initialized_at' => 'Período data Operação',
      'CompanyRating.name' => 'Classificação',
      'CertificationTsw.name' => 'Certificação TSW'
    );
  }

  public function usersEducationFormationsOptions($educationFormationGroups)
  {
    $options = array();
    foreach ($educationFormationGroups as $educationFormationGroup) {
      $options[$educationFormationGroup['EducationFormationGroup']['name']] = array();
      foreach ($educationFormationGroup['EducationFormation'] as $educationFormation) {
        $options[$educationFormationGroup['EducationFormationGroup']['name']][$educationFormation['id']] = $educationFormation['name'];
      }
    }
    return $options;
  }

  public function imageUpload($className, $field, $size)
  {
    $filename = @$this->request->data[$className][$field];
    if(empty($filename)){
      $containerClass = 'fileupload-new';
      $imgPreview = '';
    }else{
      $containerClass = 'fileupload-exists';
      $imgPreview = '<img src="'.$this->Layout->filePath($className, $field, $size).'" />';
    }
    $sizeParts = explode('x',$size);
    $width  = $sizeParts[0];
    $height = $sizeParts[1];
    
    return '
    <div data-fileupload="image" class="fileupload '.$containerClass.'"><input type="hidden" />
      <div style="width: '.$width.'px; height: '.$height.'px;" class="fileupload-new thumbnail"><img src="http://www.placehold.it/'.$size.'/EFEFEF/AAAAAA" alt="" /></div>
      <div style="width: '.$width.'px; height: '.$height.'px; line-height: '.$height.'px;" class="fileupload-preview fileupload-exists thumbnail">'.$imgPreview.'</div>
      <span class="btn btn-file"><span class="fileupload-new"><i class="icon-picture"></i> Selecionar Imagem</span><span class="fileupload-exists">Trocar</span><input type="file" name="data['.$className.']['.$field.']" id="'.$className.$field.'" /></span>
      <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remover</a>
    </div>';
  }
}
