<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', 'Lista de Companies')); ?><h3 class="heading"><?php echo __('Lista de %s', __('Companies'));?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i> Cadastrar Novo',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>
	</div>
	<div class="span8">
		<?php echo $this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo @$this->request->query['q'] ?>" />
		<?php echo $this->Form->end(NULL) ?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('manufacturer_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('fiscal_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('cnpj');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('domain');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('state_reference');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('company_type');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('created_at');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('updated_at');?></th>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
		<?php foreach ($companies as $company): ?>
			<tr>
				<td><?php echo h($company['Company']['id']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($company['Manufacturer']['name'], array('controller' => 'manufacturers', 'action' => 'view', $company['Manufacturer']['id'])); ?>
				</td>
				<td><?php echo h($company['Company']['name']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['fiscal_name']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['cnpj']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['domain']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['state_reference']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['company_type']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['created_at']); ?>&nbsp;</td>
				<td><?php echo h($company['Company']['updated_at']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $company['Company']['id']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>
					<?php echo $this->Form->postLink('<i class="icon-remove-sign align-icon"></i>', array('action' => 'delete', $company['Company']['id']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo $this->BootstrapPaginator->pagination(); ?>
		</span>

	</div>
</div>
