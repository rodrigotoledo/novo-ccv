<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Aov');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($aov['Aov']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('User'); ?></dt>
			<dd>
				<?php echo $this->Html->link($aov['User']['name'], array('controller' => 'users', 'action' => 'view', $aov['User']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($aov['Aov']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created At'); ?></dt>
			<dd>
				<?php echo h($aov['Aov']['created_at']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Updated At'); ?></dt>
			<dd>
				<?php echo h($aov['Aov']['updated_at']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Aov')), array('action' => 'edit', $aov['Aov']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Aov')), array('action' => 'delete', $aov['Aov']['id']), null, __('Are you sure you want to delete # %s?', $aov['Aov']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Aovs')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Aov')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

