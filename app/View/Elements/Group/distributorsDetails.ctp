<?php if (count(@$distributorsDetails)): ?>
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <th>Distribuidor Relacionado</th>
    <th>AOV</th>
    <th>Região</th>
    <th>Consultor Regional</th>
  </thead>
  <tbody>
    <?php foreach ($distributorsDetails as $distributor): ?>
    <tr>
      <td><?php echo $distributor['Company']['name'] ?></td>
      <td><?php echo $distributor['Aov']['name'] ?></td>
      <td><?php echo $distributor['Region']['name'] ?></td>
      <td><?php echo $distributor['UserEmployee']['name'] ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>
<?php endif ?>
