<h3 class="heading">Manutenção de usuário</h3>

<fieldset>
  <div class="row-fluid">
    <div class="span6">
    <?
    echo $this->BootstrapForm->input('email', array(
      'class' => 'span7', 'type' => 'text')
    );
    ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span6">
    <?php
    echo $this->BootstrapForm->input('name', array(
      'class' => 'span7', 'label' => 'Nome')
    );
    ?>
    </div>
    <div class="span6">
    <?
    echo $this->BootstrapForm->input('personal_email', array(
      'class' => 'span7', 'label' => 'Email pessoal')
    );
    ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span6">
    <?
    echo $this->BootstrapForm->input('pass', array(
      'class' => 'span7', 'label' => 'Senha', 'value'=>'', 'autocomplete'=>'off', 'type' => 'password')
    );
    ?>
    </div>
    <div class="span6">
    <?
    echo $this->BootstrapForm->input('confirm_password', array(
      'class' => 'span7', 'label' => 'Confirmação da senha', 'value'=>'', 'autocomplete'=>'off', 'type' => 'password')
    );
    ?>
    </div>
  </div>
  
  <div class="row-fluid">
    <div class="span6">
    <?
    echo $this->BootstrapForm->input('photo', array(
      'class' => 'span7', 'label' => 'Foto', 'type' => 'file')
    );
    ?>
    </div>
    <div class="span6">
    <?
    echo $this->BootstrapForm->input('active', array('label' => 'Activo', 'default' => true));
    ?>
    </div>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Salvar</button>
    <?php if (isset($this->data['User']['id'])): ?>
    <?php echo $this->Html->link('Apagar', array('action' => 'delete',$this->data['User']['id']), 
    array('confirm' => 'Deseja prosseguir com esta ação?', 'class'=> 'btn btn-danger')); ?>
    <?php endif ?>
  </div>
</fieldset>