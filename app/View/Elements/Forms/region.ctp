<fieldset>
  <legend class="block-title"><?php echo __('Dados %s', __('Região')); ?></legend>
  <?php
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('name', array('label' => 'Nome'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('aov_id', array('label' => 'AOV Relacionada', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4" id="userEmployee">';
    echo '</div>';
  echo '</div>';
  ?>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>


<?php $this->start('footerScripts') ?>
<script type="text/javascript">
$(function(){
  updateUserEmployee();
  $("select#RegionAovId").change(function(){
    updateUserEmployee()
  })
})
function updateUserEmployee(){
  var id = $("select#RegionAovId").val();
  $.get('<?php echo $this->Html->url(array('action' => 'updateUserEmployee')) ?>',
    {region_id: id},
    function(data){
      $('#userEmployee').html(data);
    }
  )
}
</script>
<?php $this->end() ?>
