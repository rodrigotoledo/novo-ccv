<fieldset>
  <legend class="block-title"><?php echo __('Dados Pessoais'); ?></legend>
  <?php
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    ?>
    <label for="UserDistributorphoto" class="control-label">Foto</label>
    <div class="controls">
      <?php echo $this->SimpleForm->imageUpload('UserDistributor', 'photo', '100x100') ?>
    </div>
    <?
    echo '</div>';
    echo '<div class="span8">';
    echo $this->BootstrapForm->input('name', array('label' => 'Nome Completo', 'size' => 50));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.sex', array(
        'label' => array('text' => 'Sexo', 'class' => 'inline'),
        'options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'),
        'type' => 'radio'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.birthday', array('label' => 'Data de Nascimento', 'type' => 'text',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserContact']['birthday'])));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.rg', array('label' => 'RG'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.cpf', array('label' => 'CPF', 'class' => 'mask-cpf'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('company_id', array('label' => 'Distribuidor',
      'options' => $distributors,
      'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados de Contato'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('email', array('label' => 'Email'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('confirm_email', array('label' => 'Confirmação do Email'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.mobile', array('label' => 'Telefone Celular',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.phone_comercial', array('label' => 'Telefone Comercial',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.phone_house', array('label' => 'Telefone Residencial',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Endereço'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.zipcode', array('label' => 'CEP',
      'class' => 'mask-zipcode',
      'data-zipcode-url' => $this->Html->url(array('action' => 'addressByZipcode'))));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.state_id', array('label' => 'Estado',
      'class' => 'chzn_a',
      'data-cities-url' => $this->Html->url(array('action' => 'citiesByState')),
      'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.city_id', array('label' => 'Cidade', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.address', array('label' => 'Endereço'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.address_complement', array('label' => 'Complemento', 'size' => 20));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Vestimenta'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserGarb.tshirt_id', array('label' => 'Camiseta', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserGarb.shoe_id', array('label' => 'Calçado', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Escolaridade'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserEducationInformation.literacy', array('label' => 'Grau de Instrução',
      'options' => $this->SimpleForm->usersLiteraciesOptions(),
      'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span6">';
    echo $this->BootstrapForm->input('EducationFormation.EducationFormation', array(
      'label' => 'Formação', 'options' => $this->SimpleForm->usersEducationFormationsOptions($educationFormationGroups),
      'multiple' => 'multiple', 'class' => 'multiselect', 'selected' =>$this->SimpleForm->multiSelectAssociationIds('EducationFormation.EducationFormation'))
    );
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados Profissionais'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.admitted_at', array('label' => 'Data de Admissão na Concessionária', 'type' => 'text',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserProfessionalInformation']['admitted_at'])));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.professional_position_id', array('label' => 'Cargo', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.professional_occupation_id', array('label' => 'Atividade Principal', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('change_professional_position_at', array('label' => 'Mudança de Cargo em',
      'type' => 'text', 'disabled' => 'disabled',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserProfessionalInformation']['change_professional_position_at'])));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('last_professional_position', array('label' => 'Cargo Anterior',
      'disabled' => 'disabled',
      'value' => @$this->request->data['UserProfessionalInformation']['LastProfessionalPosition']['name']));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Experiência Anterior'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('Manufacturer.Manufacturer', array(
      'label' => 'Experiência em outras Marcas', 'multiple' => 'multiple',
      'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('Manufacturer.Manufacturer'))
    );
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('ProfessionalOtherDepartment.ProfessionalOtherDepartment', array(
      'label' => 'Experiência em outros Setores', 'options' => $professionalDepartments, 'multiple' => 'multiple',
      'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('ProfessionalOtherDepartment.ProfessionalOtherDepartment'))
    );
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.company', array('label' => 'Nome da Empresa'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.department', array('label' => 'Departamento'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.duration', array('label' => 'Duração'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.professional_position', array('label' => 'Cargo', 'type' => 'text'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Configurações'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserConfiguration.role_id', array('label' => 'Perfil', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserConfiguration.status', array('label' => 'Status',
        'disabled' => (empty($this->request->data['UserDistributor']['id']) ? 'disabled' : false),
        'default' => (empty($this->request->data['UserDistributor']['id']) ? 'Em Experiência' : null),
        'options' => $this->SimpleForm->userDistributorsStatusOptions()));
    echo '</div>';
    echo '<div class="span4 temporaryOffUserWrapper">';
      echo '<a data-toggle="modal" data-backdrop="static" href="#temporary-off" class="btn btn-warning" aria-describedby="ui-tooltip-0">Jusficativa <i class="splashy-mail_light"></i></a>';
      if(isset($validationErrors) && array_key_exists('UserTemporaryOff', $validationErrors)){
        echo '<span class="help-inline error-message">* Preencha a Justificativa</span>';
      }
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserConfiguration.see_group', array('type' => 'checkbox',
        'label' => false,
        'after' => '<label>Visualização do Grupo</label>'));
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.user_inactive_reason_id', array('label' => 'Razão', 'empty' => true));
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.user_inactive_motive_id', array('label' => 'Motivo', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>



<div class="modal hide fade" id="temporary-off">
  <div class="modal-header">
    <button class="close" data-dismiss="modal">×</button>
    <h3>Justificar Ausência</h3>
  </div>
  <div class="modal-body">
    <div class="alert alert-info">Favor justificar ausência abaixo</div>
    <div class="formSep">
      <div class="row-fluid">
        <div class="span12">
          <?php echo $this->BootstrapForm->input('UserTemporaryOff.user_temporary_off_motive_id', array('label' => 'Motivo', 'empty' => 'Selecione o Motivo')) ?>
        </div>
      </div>
    </div>

    <div class="formSep">
      <div class="row-fluid">
        <div class="span12">
          <?php echo $this->BootstrapForm->input('UserTemporaryOff.document', array('label' => 'Upload de Documento', 'type' => 'file')) ?>
        </div>
      </div>
      <?php if (!empty($this->request->data['UserTemporaryOff']['document'])): ?>
      <div class="row-fluid">
        <div class="span12">
          <div class="control-group" style="margin-bottom:0px;margin-top:-10px">
            <label class="control-label">&nbsp;</label>
            <div class="controls">
              <a href="<?php echo $this->Layout->filePath('UserTemporaryOff','document', false) ?>">Clique aqui para ver o arquivo</a>
            </div>
          </div>
        </div>
      </div>
      <?php endif ?>
    </div>

    <div class="formSep">
      <div class="row-fluid">
        <div class="span12">
          <?php echo $this->BootstrapForm->input('UserTemporaryOff.start_at', array(
            'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserTemporaryOff']['start_at']),
            'label' => 'Data Início', 'type' => 'text')) ?>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12">
          <?php echo $this->BootstrapForm->input('UserTemporaryOff.end_at', array(
            'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserTemporaryOff']['end_at']),
            'label' => 'Data Fim', 'type' => 'text')) ?>
        </div>
      </div>
    </div>

    <div class="formSep">
      <div class="row-fluid">
        <div class="span12">
          <button class="btn btn-large btn-warning ttip_r" type="button" id="save-and-close-user-temporary-off" title="A justificativa só será armazenada após salvar os dados do usuário">Salvar</button>
        </div>
      </div>
    </div>
  </div>
</div>
