<fieldset>
  <legend class="block-title"><?php echo __('Dados AOV'); ?></legend>
  <div class="row-fluid">
    <?php
    echo '<div class="span6">';
    echo $this->BootstrapForm->input('name', array(
      'label' => 'Nome',
      )
    );
    echo '</div>';
    echo '<div class="span6">';
    echo $this->BootstrapForm->input('user_id', array('label' => 'Responsável',
      'class' => 'chzn_a', 'empty' => true, 'options' => $userEmployees));
    echo '</div>';
    ?>
  </div>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>
