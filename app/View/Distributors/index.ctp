<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Empresas</a>', 'Lista de Distribuidores')); ?>
<h3 class="heading"><?php echo __('Lista de %s', __('Distribuidores'));?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i> Cadastrar Novo',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>
	</div>
	<div class="span8">
		<?php echo $this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>
		<?php echo $this->Form->input('column', array('escape' => false,
				'selected' => @$this->request->query['column'],
				'options' => $this->SimpleForm->searchDistributorsOptions()
			)) ?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo @$this->request->query['q'] ?>" />
		<?php echo $this->Form->end(NULL) ?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('Manufacturer.name',__('Marca'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('DistributorInformation.dlr_toyos',__('DLR Toyos'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name',__('Nome Fantasia'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('DistributorInformation.initialized_at', __('Data início da operação'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('CompanyContact.email', __('Email'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('CompanyContact.phone_comercial1', __('Telefone Comercial'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('CompanyRating.name', __('Classificação'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('CertificationTsw.name', __('Certificação TSW'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('CompanyConfiguration.status', __('Status'));?></th>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
		<?php foreach ($distributors as $distributor): ?>
			<tr>
				<td>
					<?php echo $this->Html->link($distributor['Manufacturer']['name'], array('controller' => 'manufacturers', 'action' => 'view', $distributor['Manufacturer']['id'])); ?>
				</td>
				<td><?php echo h($distributor['DistributorInformation']['dlr_toyos']); ?>&nbsp;</td>
				<td><?php echo h($distributor['SearchDistributor']['name']); ?>&nbsp;</td>
				<td><?php echo $this->Time->format($distributor['DistributorInformation']['initialized_at'], '%d/%m/%Y'); ?>&nbsp;</td>
				<td><?php echo h($distributor['CompanyContact']['email']); ?>&nbsp;</td>
				<td><?php echo h($distributor['CompanyContact']['phone_comercial1']); ?>&nbsp;</td>
				<td><?php echo h($distributor['CompanyRating']['name']); ?>&nbsp;</td>
				<td><?php echo h($distributor['CertificationTsw']['name']); ?>&nbsp;</td>
				<td><?php echo h($distributor['CompanyConfiguration']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $distributor['SearchDistributor']['id']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>
					<?php echo $this->Form->postLink('<i class="icon-remove-sign align-icon"></i>', array('action' => 'delete', $distributor['SearchDistributor']['id']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo $this->BootstrapPaginator->pagination(); ?>
		</span>
	</div>
</div>
