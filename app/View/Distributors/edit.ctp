<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Empresas</a>', $this->Html->link('Lista de Distribuidores', array('action' => 'index')), 'Cadastro Distribuidor')); ?>
<h3 class="heading">Empresas &raquo; Cadastro Distribuidor</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Distributor', array('class' => 'form-vertical well'));?>
			<?php echo $this->element('Forms/distributor') ?>
			<?php echo $this->BootstrapForm->hidden('Distributor.id'); ?>
			<?php echo $this->BootstrapForm->hidden('DistributorInformation.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyAddress.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyContact.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyConfiguration.id'); ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
