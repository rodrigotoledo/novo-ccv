<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', $this->Html->link('Lista de Grupos', array('action' => 'index')), 'Cadastro Grupo')); ?>

<h3 class="heading">Cadastro Grupo</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Group', array('class' => 'form-vertical well'));?>
			<?php echo $this->element('Forms/group') ?>
			<?php echo $this->BootstrapForm->hidden('id'); ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
